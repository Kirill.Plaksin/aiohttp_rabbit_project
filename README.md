# aiohttp and rabbit aio async server.

How to set up?
-
`docker-compose up`

API
-
example POST: http://0.0.0.0:5000/key-value

example request body: `{"key": "test", "value": 1}` 

example GET: http://0.0.0.0:5000/key-value/?q=test

example response:
`{
  "value": "1"
}`
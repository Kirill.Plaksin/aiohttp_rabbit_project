"""HTTP view."""
import json

from aiohttp import web
from aiohttp.web_response import Response
from marshmallow.exceptions import ValidationError

from rpc_client import KeyValueRpcClient
from publisher import Publisher
from schemas import KeyValueSchema


class KeyValueView(web.View):

    async def get(self, **kwargs) -> Response:
        key = self.request.query.get('q')
        response = await KeyValueRpcClient().call(key=key)
        if response:
            return Response(status=200, body=json.dumps({'value': response.decode('utf-8')}))
        return Response(status=404, body=json.dumps({'errors': 'Not found'}), content_type='application/json')

    async def post(self, **kwargs) -> Response:
        data = await self.request.json()
        try:
            key_value = KeyValueSchema().load(data)
        except ValidationError as e:
            return Response(
                status=400,
                body=json.dumps({'status': 'fail', 'errors': e.messages}),
                content_type='application/json'
            )
        else:
            key = data['key']
            response = await KeyValueRpcClient().call(key=key)
            if not response:
                await Publisher(host='rabbitmq3', login='guest', password='guest', port=5672).task(key_value)
                return Response(status=201, body=json.dumps({'status': 'success'}), content_type='application/json')
            else:
                return Response(
                    status=400,
                    body=json.dumps({'error': f'found duplicate of the key "{key}"'}),
                    content_type='application/json'
                )

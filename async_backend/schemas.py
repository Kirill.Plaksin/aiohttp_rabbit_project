"""Schemas."""
from marshmallow import (
    Schema,
    fields
)


class KeyValueSchema(Schema):
    key = fields.Str(required=True)
    value = fields.Int(required=True)

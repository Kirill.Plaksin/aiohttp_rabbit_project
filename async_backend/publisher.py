"""Implementation of Rabbit publisher"""
import json
import aioamqp


class Publisher:
    def __init__(self, host, port, login, password):
        self.host = host
        self.port = port
        self.login = login
        self.password = password

    async def connection(self):
        try:
            return await aioamqp.connect(
                host=self.host,
                port=self.port,
                login=self.login,
                password=self.password
            )
        except aioamqp.AmqpClosedConnection:
            return

    async def task(self, data):
        transport, protocol = await self.connection()
        channel = await protocol.channel()
        await channel.queue('task_queue', durable=True)
        await channel.basic_publish(
            payload=json.dumps(data),
            exchange_name='',
            routing_key='task_queue',
        )
        await protocol.close()
        transport.close()

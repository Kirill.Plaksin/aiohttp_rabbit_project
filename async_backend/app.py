"""app run server."""
from aiohttp.web import (
    Application,
    run_app,
)

from views import KeyValueView

if __name__ == '__main__':
    app = Application()
    app.router.add_post('/key-value', KeyValueView)
    app.router.add_get('/key-value/', KeyValueView)
    run_app(app, port=5000)

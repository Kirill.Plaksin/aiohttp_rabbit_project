"""Run receiver and rpc server."""
import asyncio

from receiver import Worker
from rpc_server import RpcServer

loop = asyncio.get_event_loop()
loop.create_task(
    Worker(host='rabbitmq3', login='guest', password='guest', port=5672, queue_name='task_queue').run()
)
loop.run_until_complete(RpcServer().run())
print(' [*] Waiting for logs. To exit press CTRL+C')
loop.run_forever()

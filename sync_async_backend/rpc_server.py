"""Implementation of RPC server. """
import aioamqp
import aiofiles
from aiocsv import AsyncReader


class RpcServer:
    async def connection(self):
        try:
            return await aioamqp.connect(host='rabbitmq3')
        except aioamqp.AmqpClosedConnection:
            return

    async def on_request(self, channel, body, envelope, properties):
        response = await self._get_value_by_key(key=body.decode('utf-8'))
        await channel.basic_publish(
            payload=str(response),
            exchange_name='',
            routing_key=properties.reply_to,
            properties={
                'correlation_id': properties.correlation_id,
            },
        )

        await channel.basic_client_ack(delivery_tag=envelope.delivery_tag)

    async def _get_value_by_key(self, key: str):
        async with aiofiles.open("db.csv", mode="r", encoding="utf-8", newline="") as afp:
            async for row in AsyncReader(afp, delimiter="\t"):
                file_key, file_value = row[0].split(',')
                file_value = ''.join([i for i in file_value if i.isdigit()])
                if file_key == key:
                    return int(file_value)
        return ''

    async def run(self):
        transport, protocol = await self.connection()
        channel = await protocol.channel()
        await channel.queue_declare(queue_name='rpc_queue')
        await channel.basic_qos(prefetch_count=1, prefetch_size=0, connection_global=False)
        await channel.basic_consume(self.on_request, queue_name='rpc_queue')

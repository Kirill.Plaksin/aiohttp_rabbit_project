"""Implementation of Receiver."""

import json
import aioamqp
import csv

import aiofiles
from aiocsv import AsyncDictWriter


class Worker:
    def __init__(self, host, port, login, password, queue_name):
        self.host = host
        self.port = port
        self.login = login
        self.password = password
        self.queue_name = queue_name

    async def connection(self):
        try:
            return await aioamqp.connect(
                host=self.host,
                port=self.port,
                login=self.login,
                password=self.password
            )
        except aioamqp.AmqpClosedConnection:
            return

    async def callback(self, channel, body, envelope, properties):
        json_data = json.loads(body)
        async with aiofiles.open("db.csv", mode="a", encoding="utf-8", newline="") as afp:
            writer = AsyncDictWriter(afp, ["key", "value"], restval="NULL", quoting=csv.QUOTE_ALL)
            await writer.writerow(json_data)
        await channel.basic_client_ack(delivery_tag=envelope.delivery_tag)

    async def run(self):
        transport, protocol = await self.connection()
        channel = await protocol.channel()
        await channel.queue(queue_name=self.queue_name, durable=True)
        await channel.basic_qos(prefetch_count=1, prefetch_size=0, connection_global=False)
        await channel.basic_consume(callback=self.callback, queue_name=self.queue_name)
